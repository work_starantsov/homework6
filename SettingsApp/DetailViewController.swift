//
//  DetailViewController.swift
//  SettingsApp
//
//  Created by Nazar Starantsov on 25.10.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController: UIViewController {
    private var cellData: SettingsCellData!
    private var indexPath: IndexPath!
    
    var delegate: TableViewDetailDelegate?
    
    lazy var textView: UITextView = {
        let text = UITextView()
        text.font = UIFont.systemFont(ofSize: 19, weight: .bold)
        text.backgroundColor = UIColor.cyan.withAlphaComponent(0.5)
        text.layer.cornerRadius = 15
        text.textAlignment = .center
        return text
    }()
    
    convenience init() {
        self.init(cellData: nil, indexPath: nil)
    }

    init(cellData: SettingsCellData?, indexPath: IndexPath?) {
        super.init(nibName: nil, bundle: nil)
        self.cellData = cellData
        self.indexPath = indexPath
        self.textView.text = cellData?.title
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        setupSubviews()
        setupConstraints()
    }
    
    func setupSubviews() {
        view.addSubview(textView)
        textView.delegate = self
    }
    
    func setupConstraints() {
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        textView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        textView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6).isActive = true
        textView.heightAnchor.constraint(equalToConstant: 100).isActive = true
    }
}

extension DetailViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        self.delegate?.changeTitleOfItemAt(indexPath: self.indexPath, to: textView.text)
    }
}

protocol TableViewDetailDelegate {
    func changeTitleOfItemAt(indexPath: IndexPath, to title: String)
}
