//
//  TableViewController.swift
//  SettingsApp
//
//  Created by Nazar Starantsov on 25.10.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import UIKit

class TableViewController: UIViewController {
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: UITableView.Style.grouped)
        return tableView
    }()
    
    var items: [BaseSectionData] = [
        BaseSectionData(sectionTitle: "Big cell", cellItems: [BigCellData(title: "Sign in to your phone", subtitle: "Set up iCloud, the App Store, and more", imageName: "person")]),
        BaseSectionData(sectionTitle: "S1", cellItems: [
            BasicCellData(title: "General", imageName: "general"),
            BasicCellData(title: "Contacts", imageName: "contacts")
        ]),
        BaseSectionData(sectionTitle: "S2", cellItems: [
            BasicCellData(title: "App Store", imageName: "appstore")
        ]),
        BaseSectionData(sectionTitle: "S3", cellItems: [
             BasicCellData(title: "Maps", imageName: "maps"),
             BasicCellData(title: "Safari", imageName: "safari"),
             BasicCellData(title: "News", imageName: "news")
        ])
    ]
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = tableView.backgroundColor
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = .white
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Settings"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        setupSubviews()
        registerCells()
        setupConstraints()
        setupDelegates()
    }

    func setupSubviews() {
        view.addSubview(tableView)
    }
    
    func registerCells() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "baseCell")
        tableView.register(UITableViewSubtitleCell.self, forCellReuseIdentifier: "bigCell")
    }
    
    func setupConstraints() {
        tableView.bounces = false
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    func setupDelegates() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize)-> UIImage? {

        UIGraphicsBeginImageContext(newSize)
        
        image.draw(in: CGRect(x: 0,y: 0,width: newSize.width,height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage?.withRenderingMode(.alwaysOriginal)
    }
}

extension TableViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 65 : 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
               cell?.isSelected = false
        
        let detailVC = DetailViewController(cellData: items[indexPath.section].cellItems[indexPath.item], indexPath: indexPath)
        detailVC.view.backgroundColor = .white
        detailVC.delegate = self
       
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}

extension TableViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].cellItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "bigCell"),
                let cellData = items[indexPath.section].cellItems[indexPath.item] as? BigCellData else {
                    
                return UITableViewCell()
            }
            
            cell.textLabel?.text = cellData.title
            cell.textLabel?.textColor = .systemBlue
            cell.detailTextLabel?.text = cellData.subtitle
            cell.detailTextLabel?.textColor = .systemGray
            cell.imageView?.image = UIImage(named: cellData.imageName)
            cell.imageView?.tintColor = .systemGray

            cell.accessoryType = .disclosureIndicator
            
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "baseCell") else {
                return UITableViewCell()
            }
            let cellData = items[indexPath.section].cellItems[indexPath.item]
            
            cell.textLabel?.text = cellData.title
            cell.imageView?.image = imageWithImage(image: UIImage(named: cellData.imageName)!, scaledToSize: CGSize(width: 28, height: 28))
            cell.accessoryType = .disclosureIndicator
            
            return cell
        }
    }
    
    
}

extension TableViewController: TableViewDetailDelegate {
    func changeTitleOfItemAt(indexPath: IndexPath, to title: String) {
        // CODE TO CHANGE TITLE
        items[indexPath.section].cellItems[indexPath.item].title = title
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
}


protocol SettingsCellData {
    var title: String { get set }
    var imageName: String { get }
}

struct BasicCellData: SettingsCellData {
    var title: String
    let imageName: String
}

struct BigCellData: SettingsCellData {
    var title: String
    var subtitle: String
    let imageName: String
}

struct BaseSectionData {
    var sectionTitle: String
    var cellItems: [SettingsCellData]
}
